package service.beans;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement
public class Products {

    ArrayList<ProductsCategory> productsCategory;

    public ArrayList<ProductsCategory> getProductsCategory() {
        return productsCategory;
    }

    public void setProductsCategory(ArrayList<ProductsCategory> productsCategory) {
        this.productsCategory = productsCategory;
    }
}
