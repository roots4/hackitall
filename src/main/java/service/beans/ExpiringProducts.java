package service.beans;

import java.util.HashMap;
import java.util.Map;

public class ExpiringProducts {

    public Map<Integer, Long> products;

    public ExpiringProducts(){
        products = new HashMap<Integer, Long>();
    }

    public void addTimestamp(int id, Long timestamp){
        if(!products.containsKey(id)) {
            products.put(id, timestamp);
            return;
        }
        products.put(id, Math.min(timestamp, products.get(id)));

    }

    public Long getExpTime(int id){
        if(!products.containsKey(id))
            return null;
        return products.get(id);
    }

}
