package service.beans;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UserProductBean {

    String productName;
    String userId;
    String productId;
    String expDate;
    String quantity;

    public String getProductName() { return productName; }

    public void setProductName(String productName) { this.productName = productName; }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getExpDate() {
        return expDate;
    }

    public void setExpDate(String expDate) {
        this.expDate = expDate;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String toString(){
        return "[" + userId + ", " + productId + ", " + expDate + ", " + quantity + "]";
    }
}
