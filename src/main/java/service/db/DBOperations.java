package service.db;

import com.google.cloud.datastore.*;
import service.beans.*;
import service.rest.response.Recipe;
import service.rest.utils.StackOperations;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import service.rest.utils.Stack;


public class DBOperations {

    private static final Datastore datastore = DatastoreOptions.getDefaultInstance().getService();

    private static final KeyFactory userProductsKeyFactory = datastore.newKeyFactory().setKind("UserProducts");
    private static final KeyFactory recipesIntentsKeyFactory = datastore.newKeyFactory().setKind("RecipesIntents");

    public static Long addUserProduct(UserProductBean obj) {

        try {

            String productName = obj.getProductName();

            Query<Entity> query1 = Query.newEntityQueryBuilder()
                    .setKind("Products")
                    .setFilter(StructuredQuery.PropertyFilter.eq("ProductName", productName))
                    .build();

            QueryResults<Entity> products = datastore.run(query1);

            System.out.println("got product");



            if (products.hasNext()) {
                Entity product = products.next();
                String  productId = product.getString("ProductId");

                System.out.println("productId = " + productId);

                Key key = datastore.allocateId(userProductsKeyFactory.newKey());
                Entity entity = Entity.newBuilder(key)
                        .set("userId", obj.getUserId())
                        .set("productId", productId)
                        .set("expDate", (new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).parse(obj.getExpDate())).getTime())
                        .set("quantity", obj.getQuantity())
                        .build();
                datastore.put(entity);

                return key.getId();
            }
            return null;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }


        //initial version

        //        try {
//
//            Key key = datastore.allocateId(userProductsKeyFactory.newKey());
//            Entity entity = Entity.newBuilder(key)
//                    .set("userId", obj.getUserId())
//                    .set("productId", obj.getProductId())
//                    .set("expDate", (new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH).parse(obj.getExpDate())).getTime())
//                    .set("quantity", obj.getQuantity())
//                    .build();
//            datastore.put(entity);
//
//            return key.getId();
//        } catch (Exception e) {
//            e.printStackTrace();
//            return null;
//        }
    }

    public static void deleteUserProduct(Long id){
        datastore.delete(userProductsKeyFactory.newKey(id));
    }

    public static Products getAllProducts() {

        System.out.println("getAllProducts");

        Query<ProjectionEntity> query = Query.newProjectionEntityQueryBuilder()
                .setKind("Products")
                .setProjection("ProductCategory")
                .setDistinctOn("ProductCategory")
                .build();

        QueryResults<ProjectionEntity> tasks = datastore.run(query);

        System.out.println("got ProductsCategory");

        ArrayList<ProductsCategory> productsCategoryList = new ArrayList<ProductsCategory>();

        while (tasks.hasNext()) {
            ProjectionEntity task = tasks.next();
            String cat = task.getString("ProductCategory");

            Query<Entity> query1 = Query.newEntityQueryBuilder()
                    .setKind("Products")
                    .setFilter(StructuredQuery.PropertyFilter.eq("ProductCategory", cat))
                    .build();

            QueryResults<Entity> products = datastore.run(query1);

            System.out.println("got products");

            ArrayList<Product> productList = new ArrayList<Product>();

            while (products.hasNext()) {
                System.out.println("for every product");
                Entity product = products.next();

                Product productResponse = new Product();
                productResponse.setProductCategory(product.getString("ProductCategory"));
                productResponse.setProductId(product.getString("ProductId"));
                productResponse.setProductName(product.getString("ProductName"));

                productList.add(productResponse);
            }

            ProductsCategory productsCategory = new ProductsCategory();
            productsCategory.setCategory(cat);
            productsCategory.setProducts(productList);

            productsCategoryList.add(productsCategory);

        }

        Products productsResponse = new Products();

        productsResponse.setProductsCategory(productsCategoryList);

        return productsResponse;

    }

    public static ProductsCategory getUserProducts(String userId) {

        System.out.println("getUserProducts");

        Query<Entity> query1 = Query.newEntityQueryBuilder()
                .setKind("UserProducts")
                .setFilter(StructuredQuery.PropertyFilter.eq("userId", userId))
                .build();

        QueryResults<Entity> userProducts = datastore.run(query1);

        System.out.println("got products");

        ArrayList<Product> productList = new ArrayList<Product>();

        while (userProducts.hasNext()) {
            System.out.println("for every user product");
            Entity product = userProducts.next();

            Product productResponse = new Product();

            Timestamp timeStamp = new Timestamp(product.getLong("expDate")));
            Date date = new java.sql.Date(timeStamp.getTime());

            productResponse.setExpDate(String.valueOf(date));
            productResponse.setQuantity(product.getString("quantity"));

            Query<Entity> query2 = Query.newEntityQueryBuilder()
                    .setKind("Products")
                    .setFilter(StructuredQuery.PropertyFilter.eq("ProductId", product.getString("productId")))
                    .build();

            QueryResults<Entity> products = datastore.run(query2);
            if (products.hasNext()) {
                System.out.println("found product in Products");
                Entity userProduct = products.next();
                productResponse.setProductName(userProduct.getString("ProductName"));
            }

            productList.add(productResponse);
        }

        ProductsCategory productsCategory = new ProductsCategory();
        productsCategory.setProducts(productList);

        return productsCategory;

    }

    public static Map<Integer, List<Integer>> getPossibleRecipes(){
        Map<Integer, List<Integer>> rasp = new HashMap<Integer, List<Integer>>();

        Query<Entity> query = Query.newEntityQueryBuilder()
                .setKind("PossibleRecipes")
                .build();

        QueryResults<Entity> recipes = datastore.run(query);

        while(recipes.hasNext()){
            Entity entity = recipes.next();
            Integer recipeId = ((int) entity.getLong("recipeId"));

            System.out.println("recipeId: " + recipeId);

            rasp.put(recipeId, getRecipeIngredients(recipeId + ""));
        }

        return rasp;
    }

    public static List<Integer> getRecipeIngredients(String recipeId){
        List<Integer> list = new ArrayList<Integer>();

        Query<Entity> query = Query.newEntityQueryBuilder()
                .setKind("Recipes")
                .setFilter(StructuredQuery.PropertyFilter.eq("RecipeId", recipeId))
                .build();
        Entity entity = datastore.run(query).next();

        String elems[] = entity.getString("Ingredients").split(",");

        for(String elem: elems)
            list.add(Integer.parseInt(elem));

        System.out.println("Ingredients list: " + list);

        return list;
    }

    public static ExpiringProducts getExpiringProducts(String userId){
        ExpiringProducts products = new ExpiringProducts();

        Query<Entity> query = Query.newEntityQueryBuilder()
                .setKind("UserProducts")
                .setFilter(StructuredQuery.PropertyFilter.eq("userId", userId))
                .build();

        QueryResults<Entity> allProducts = datastore.run(query);

        while(allProducts.hasNext()){
            Entity entity = allProducts.next();

            products.addTimestamp(Integer.parseInt(entity.getString("productId")), entity.getLong("expDate"));
        }
        System.out.println("Products: " + products.products);
        return products;
    }

    public static Recipe getRecipeDetails(int id){
        Recipe recipe = new Recipe();

        recipe.setRecipeGlobalId("" + id);

        Query<Entity> query = Query.newEntityQueryBuilder()
                .setKind("Recipes")
                .setFilter(StructuredQuery.PropertyFilter.eq("RecipeId", String.valueOf(id)))
                .build();

        System.out.println("id: " + id);

        QueryResults<Entity> detailsSet = datastore.run(query);

        if(detailsSet.hasNext()) {
            Entity details = detailsSet.next();

            recipe.setDescription(details.getString("Description"));
            recipe.setRecipeName(details.getString("RecipeName"));
        } else {
            System.out.println("result empty");
        }

        return recipe;
    }

    public static void addUserToRecipeIntent(String recipeId, String userId){

        Set<Integer> ingredients = getUserIngredients(userId);
        ArrayList<Stack> stack = StackOperations.getStack(getRecipeIntention(recipeId));

        for(Stack elem: stack){
            if(ingredients.contains(Integer.parseInt(elem.getIngredient()))){
                elem.addUser(userId);
            }
        }

        Query<Entity> query = Query.newEntityQueryBuilder()
                .setKind("RecipesIntents")
                .setFilter(StructuredQuery.PropertyFilter.eq("RecipeId", recipeId))
                .build();
        Entity entity = datastore.run(query).next();


        Entity task = Entity.newBuilder(datastore.get(entity.getKey())).set("queues", StackOperations.getString(stack)).build();
        datastore.update(task);
    }

    public static String getRecipeIntention(String recipeId){

        Query<Entity> query = Query.newEntityQueryBuilder()
                .setKind("RecipesIntents")
                .setFilter(StructuredQuery.PropertyFilter.eq("recipeId", recipeId))
                .build();

        QueryResults<Entity> recipes = datastore.run(query);

        if(recipes.hasNext()) {
            Entity entity = recipes.next();
            return entity.getString("queues");
        }

        return null;
    }

    public static Set<Integer> getUserIngredients(String userId){
        Set<Integer> set = new TreeSet<Integer>();

        Query<Entity> query = Query.newEntityQueryBuilder()
                .setKind("UserProducts")
                .setFilter(StructuredQuery.PropertyFilter.eq("userId", userId))
                .build();

        QueryResults<Entity> ingredients = datastore.run(query);

        while(ingredients.hasNext()) {
            Entity entity = ingredients.next();
            set.add(Integer.parseInt(entity.getString("productId")));
        }

        return set;
    }

}
