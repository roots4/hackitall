package service.rest;


import com.fasterxml.jackson.databind.ObjectMapper;
import service.beans.Products;
import service.beans.ProductsCategory;
import service.beans.UserProductBean;
import service.db.DBOperations;
import service.rest.response.GenericResponse;
import service.rest.response.Recipes;
import service.rest.utils.Utils;

import javax.print.attribute.standard.Media;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/services")
public class Services {


    //-------------TEST---------------

    @POST
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/openid")
    public Response getMyID(@FormParam("name") String name) {

        GenericResponse response = new GenericResponse();
        response.setResponse("MERGE");

        // success HTTP 200
        return Response.status(Response.Status.OK)
                .entity(response)
                .build();
    }


    //---------------------------------

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/addUserProduct")
    public Response addUserProduct(UserProductBean obj){
        System.out.println(obj);

        Long resp = DBOperations.addUserProduct(obj);
        if(resp == null){
            return Utils.buildErrorMsg("Error inserting in db.");
        }

        return Utils.buildInsertSuccessResponse(resp.toString());
    }

    @DELETE
    @Path("/deleteProduct/{id}")
    public Response deleteUserProduct(@PathParam("id") String id){
        DBOperations.deleteUserProduct(Long.parseLong(id));
        return Utils.buildEmptySuccessResponse();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getAllProducts")
    public Response getAllProducts(){

        Products resp = DBOperations.getAllProducts();
        if(resp == null){
            return Utils.buildErrorMsg("Error getting products");
        }

        return Response.status(Response.Status.OK)
                .entity(resp)
                .build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getUserProducts/{userId}")
    public Response getUserProducts(@PathParam("userId") String userId){

        ProductsCategory resp = DBOperations.getUserProducts(userId);
        if(resp == null){
            return Utils.buildErrorMsg("Error getting products");
        }

        return Response.status(Response.Status.OK)
                .entity(resp)
                .build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/getRecipes/{userId}")
    public Response getRecipes(@PathParam("userId") String userId){
        Recipes recipes = Utils.getPossibleRecipes(userId);

        if(recipes == null){
            return Utils.buildErrorMsg("Error getting recepies");
        }
        ObjectMapper mapper = new ObjectMapper();

        try {
            return Response.status(Response.Status.OK)
                    .entity(mapper.writeValueAsString(recipes))
                    .build();
        } catch(Exception e){
            return null;
        }
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/registerIntention/{userId}/{recipeId}")
    public Response registerIntention(@PathParam("userId") String userId,
                                      @PathParam("recipeId") String recipeId)
    {

        DBOperations.addUserToRecipeIntent(userId, recipeId);
        return Response.status(200).build();

    }

}
