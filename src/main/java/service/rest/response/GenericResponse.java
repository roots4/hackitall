package service.rest.response;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class GenericResponse {

    String response;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
