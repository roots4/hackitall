package service.rest.response;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Recipe {
    //private String recipeId;
    private String recipeGlobalId;
    private String recipeName;
    private String description;

   /* public String getRecipeId() {
        return recipeId;
    }*/

    public String getRecipeGlobalId() {
        return recipeGlobalId;
    }

    public void setRecipeGlobalId(String recipeGlobalId) {
        this.recipeGlobalId = recipeGlobalId;
    }

   /* public void setRecipeId(String recipeId) {
        this.recipeId = recipeId;
    }*/

    public String getDescription() {
        return description;
    }

    public String getRecipeName() {
        return recipeName;
    }

    public void setRecipeName(String recipeName) {
        this.recipeName = recipeName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
