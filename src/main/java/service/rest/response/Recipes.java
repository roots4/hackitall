package service.rest.response;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class Recipes {

    private ArrayList<Recipe> recipesList;

    public ArrayList<Recipe> getRecipes() {
        return recipesList;
    }

    public void setRecipes(ArrayList<Recipe> recipesList) {
        this.recipesList = recipesList;
    }

}
