package service.rest.utils;


import com.google.gson.JsonObject;
import service.beans.ExpiringProducts;
import service.db.DBOperations;
import service.rest.response.Recipe;
import service.rest.response.Recipes;

import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

public class Utils {

    public static Response buildErrorMsg(String msg){
        JsonObject json = new JsonObject();
        json.addProperty("err", msg);

        return Response.status(400).entity(json.toString()).build();
    }

    public static Response buildInsertSuccessResponse(String id){
        JsonObject json = new JsonObject();
        json.addProperty("id", id);

        return Response.status(200).entity(json.toString()).build();
    }

    public static Response buildEmptySuccessResponse(){
        return Response.status(200).build();
    }

    public static Recipes getPossibleRecipes(String userId){

        Map<Integer, List<Integer>> possibleRecipes = DBOperations.getPossibleRecipes();
        ExpiringProducts products = DBOperations.getExpiringProducts(userId);

        List<Integer> recipes = getOrderedRecipes(possibleRecipes, products);

        return createRecipesList(recipes);
    }

    public static List<Integer> getOrderedRecipes(Map<Integer, List<Integer>> recipes, ExpiringProducts expProducts){
        Map<Integer, Integer> recipesCount = new HashMap<Integer, Integer>();
        try {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            long time = (new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS", Locale.ENGLISH).parse(timestamp.toString())).getTime();

            for(int id: recipes.keySet()){
                int count = 0;

                for(int ingredientId: recipes.get(id)){
                    if(expProducts.getExpTime(ingredientId) == null)
                        continue;
                    else if(time > (expProducts.getExpTime(ingredientId) - 259200))
                        count += 3;
                    else
                        count++;
                }

                recipesCount.put(id, count);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
        System.out.println("recipes count: " + recipesCount);

        System.out.println("soreted list: " + convertToSortedList(recipesCount));

        return convertToSortedList(recipesCount);
    }

    public static Recipes createRecipesList(List<Integer> elems){
        Recipes recipes = new Recipes();
        ArrayList<Recipe> recipesArr = new ArrayList<Recipe>();

        for(int elem: elems){
            recipesArr.add(DBOperations.getRecipeDetails(elem));
        }

        recipes.setRecipes(recipesArr);

        return recipes;
    }

    public static List<Integer> convertToSortedList(Map<Integer, Integer> recipes){
        Set<Map.Entry<Integer,Integer>> mapEntries = recipes.entrySet();

        List<Map.Entry<Integer,Integer>> aList = new LinkedList<Map.Entry<Integer,Integer>>(mapEntries);

        // sorting the List
        Collections.sort(aList, new Comparator<Map.Entry<Integer,Integer>>() {

            @Override
            public int compare(Map.Entry<Integer, Integer> ele1,
                               Map.Entry<Integer, Integer> ele2) {

                return ele1.getValue().compareTo(ele2.getValue());
            }
        });

        List<Integer> finalList = new ArrayList<Integer>();

        for(Map.Entry<Integer, Integer> entity: aList){
            finalList.add(entity.getKey());
        }

        return finalList;
    }

}
