package service.main;

import com.sun.jersey.api.container.grizzly2.GrizzlyServerFactory;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
import org.glassfish.grizzly.http.server.HttpServer;

public class Main {

    public static void main(String args[]) {
        try {
            ResourceConfig rc = new PackagesResourceConfig("service.rest");
            HttpServer server = GrizzlyServerFactory.createHttpServer("http://0.0.0.0:80", rc);

            server.start();
            System.out.println("Press any key to stop the server...");
            System.in.read();
        } catch (Exception e) {
            System.err.println(e);
        }
    }
}
